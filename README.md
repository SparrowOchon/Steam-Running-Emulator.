# Steam Running Emulator.
#### The following is a application execution emulator for steam.This will allow you to keep any application 'Running' for long periods of time while taking as little resources as possible.

## Installation
1. Compile the donothing.c Tested with `GCC` and `Microsoft C++`
2. Navigate to the steam executable folder for the application you wish to emulate running.
3. Replace the Executable in said folder with the one created in Step 1). **Make sure the name of the applications are the same.**
3. Execute the application from steam. Nothing will show up but it will say you are running the application.

## NOTE:
* Compile for the platform you intend on using this on.
* Make a **BACKUP** of the application you are replacing.